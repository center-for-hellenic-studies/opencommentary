from graphene_django.types import DjangoObjectType

from alexandria_app.models import RevisionComment

class RevisionCommentType(DjangoObjectType):
    class Meta:
        model = RevisionComment