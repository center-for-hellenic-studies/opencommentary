# this file is named authors to avoid clash of many to many association name space with project.commenters
from graphene_django.types import DjangoObjectType

from alexandria_app.models import CustomUser

class CustomUserType(DjangoObjectType):
    class Meta:
        model = CustomUser