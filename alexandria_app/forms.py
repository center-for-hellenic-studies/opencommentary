from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from tinymce.widgets import TinyMCE

from .models import CustomUser, Project

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'bio', 'tagline')

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'bio', 'tagline')

class ProjectForm(forms.ModelForm):
    hostname = forms.CharField(required=False)
    title = forms.CharField(required=False)
    about_html = forms.CharField(required=False, widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
    class Meta:
        model = Project
        fields = ('hostname', 'title', 'privacy', 'about_html', 'admins', 'editors', 'commenters' )

class TextForm(forms.Form):
    text_file = forms.FileField(label='Select a text XML file')