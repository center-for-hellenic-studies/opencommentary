from django.test import TestCase

from alexandria_app.lib import parse_urn


class ParseUrnTest(TestCase):
    def test_range_contains_passage(self):
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [1, 1], 'to': [1, 2]}, [1, 1]))
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [1, 1], 'to': [1, 2]}, [1, 1]))
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [1, 1, 1], 'to': [2, 1, 1]}, [1, 3, 1]))
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [2, 3, 4], 'to': [2, 3, 24]}, [2, 3, 23]
        ))
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [2, 5, 6], 'to': [2, 5, 6]}, [2, 5, 6]
        ))
        self.assertTrue(parse_urn.range_contains_passage(
            {'from': [6, 170], 'to': [6, 199]}, [6, 180]
        ))

    def test_range_does_not_contain_passage(self):
        self.assertFalse(parse_urn.range_contains_passage(
            {'from': [2, 3, 4], 'to': [2, 3, 24]}, [2, 3, 25]
        ))
        self.assertFalse(parse_urn.range_contains_passage(
            {'from': [1, 1], 'to': [1, 2]}, [1, 3]))
        self.assertFalse(parse_urn.range_contains_passage(
            {'from': [6, 170], 'to': [6, 199]}, [1, 180]
        ))

    def test_is_passage_in_range(self):
        self.assertTrue(parse_urn.is_passage_in_range(
            {'from': [1, 1], 'to': [1, 2]}, {'from': False, 'to': [2, 1]}))
        self.assertTrue(parse_urn.is_passage_in_range(
            {'from': [5], 'to': [5]},
            {'from': [1], 'to': [30]}))
        self.assertTrue(parse_urn.is_passage_in_range(
            {'from': [2, 5], 'to': [2, 5]},
            {'from': [1, 1], 'to': [2, 30]}))
