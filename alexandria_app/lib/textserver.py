"""[summary]

Returns:
    [type] -- [description]
"""
import json
from django.conf import settings
from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport


class Textserver(object):
    def __init__(self):
        self.client = Client(
            transport=RequestsHTTPTransport(
                url=settings.TEXTSERVER or 'https://textserver.newalexandria.info/graphql')
        )

    def fetch_textnodes(self, urn):
        query = gql('''
            query fetch_textnodes($urn: CtsUrn!) {
                textNodes(
                    urn: $urn
                    pageSize: 1000
                ) {
                    id
                    text
                    urn
                }
            }
        ''')

        params = { "urn": urn }

        return self.client.execute(query, variable_values=params)

    def search_works(self, search_term):
        query = gql('''
            query searchWorks(textsearch: String!) {
                workSearch(
                    textsearch: $textsearch
                    limit: 0
                ) {
                    total
                    works {
                        id
                        english_title
                        original_title
                        description
                        structure
                        form
                        urn
                        full_urn
                        work_type
                        label
                        language {
                            id
                            title
                        }
                        textGroupID
                    }
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''')

        params = { "textsearch": search_term }

        return self.client.execute(query, variable_values=params)

    def fetch_works_by_urn_list(self, urn_list):
        query = gql('''
            query fetchWorksByUrnList($urns: [String]!) {
                workSearch(
                    urns: $urns
                    limit: 0
                ) {
                    total
                    works {
                        id
                        english_title
                        original_title
                        description
                        structure
                        form
                        urn
                        full_urn
                        work_type
                        label
                        language {
                            id
                            title
                        }
                        textGroupID
                    }
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''')

        params = { "urns": urn_list }

        return self.client.execute(query, variable_values=params)

    def fetch_works_by_urn(self, full_urn):
        query = gql('''
            query fetchWorksByUrn($urn: String!) {
                workByUrn(
                    full_urn: $urn
                ) {
                    id
                    slug
                    english_title
                    original_title
                    form
                    work_type
                    label
                    description
                    urn
                    full_urn
                    language {
                        id
                        title
                        slug
                    }
                    version {
                        id
                        slug
                        title
                        description
                    }
                    exemplar {
                        id
                        slug
                        title
                        description
                    }
                    refsDecls {
                        description
                        label
                    }
                    translation {
                        id
                        slug
                        title
                        description
                    }
                    textNodes {
                        id
                        index
                        location
                        text
                        urn
                    }
                }
            }
        ''')

        params = { "urn": full_urn }

        return self.client.execute(query, variable_values=params)

    def fetch_work_by_id(self, work_id):
        query = gql('''
            query fetchWorkById($id: ID!) {
                work(
                    id: $id
                ) {
                    id
                    english_title
                    original_title
                    language {
                        id
                        title
                    }
                    urn
                    full_urn
                }
            }
        ''' % work_id)

        params = { "id": work_id }

        return self.client.execute(query, variable_values=params)

    def fetch_textgroups(self, urn):
        query = gql('''
            query fetchTextGroups($urn: CtsUrn!) {
                textGroups(
                    urn: $urn
                ) {
                    id
                    title
                    urn
                }
            }
        ''')

        params = { "urn": urn }

        return self.client.execute(query, variable_values=params)

    def fetch_works(self, urn):
        query = gql('''
            query fetchWorks($urn: CtsUrn!) {
                works(
                    urn: $urn
                ) {
                    id
                    english_title
                    original_title
                    urn
                    full_urn
                    work_type
                    description
                    language {
                        id
                        title
                        slug
                    }
                }
            }
        ''')

        params = { "urn": urn }

        return self.client.execute(query, variable_values=params)

    def fetch_all_works(self):
        query = gql('''
            {
                works (
                    limit: 0
                ) {
                    id
                    english_title
                    original_title
                    description
                    structure
                    form
                    urn
                    full_urn
                    work_type
                    label
                    language {
                        id
                        title
                        slug
                    }
                    textGroupID
                }
                languages(
                    limit: 65535
                ) {
                    id
                    title
                    slug
                }
                collections(
                    limit: 65535
                ) {
                    id
                    title
                }
                textGroups(
                    limit: 65535
                ) {
                    id
                    title
                    collectionId
                }
            }
        ''')
        return self.client.execute(query)

    def fetch_search_facets(self):
        query = gql('''
            query fetchSearchFacets {
              collections {
                id
                title
                slug
                urn
                textGroups {
                  id
                  title
                  slug
                  urn
                }
              }
              languages {
                id
                title
                slug
              }
            }
        ''')
        return self.client.execute(query)
