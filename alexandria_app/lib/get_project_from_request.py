from ..models import Project

from .get_archive_hostname import get_archive_hostname


def get_project_from_request(request):
    hostname = get_archive_hostname(request, from_origin=True)
    return Project.objects.get(hostname=hostname)
