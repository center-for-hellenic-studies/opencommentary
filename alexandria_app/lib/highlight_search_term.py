import re
from django.utils.html import strip_tags


def highlight_search_term(search_term, body_text=''):
    excerpt = ''
    body_text = strip_tags(body_text)

    if search_term:
        search_term_re = re.compile(re.escape(search_term), re.IGNORECASE)
        occurence = search_term_re.search(body_text)

        # if occurence is appearing in search revision text (and not title)
        if occurence:

            body_text = body_text[:occurence.start()] + "<span class='searchHighlight'>" + body_text[occurence.start():]
            body_text = body_text[:occurence.start()+30+len(search_term)] + "</span>" + body_text[occurence.start()+30+len(search_term):]

            if (occurence.start() - 120) > 3:
                start_offset = occurence.start() - 120
            else:
                start_offset = 0

            if (occurence.end() + 90) < len(body_text):
                end_offset = occurence.end() + 90
                if start_offset < 4:
                    end_offset += 90 - start_offset
            else:
                end_offset = len(body_text)

            if start_offset > 4:
                excerpt = "... "

            excerpt += body_text[start_offset:end_offset]

            if end_offset < len(body_text) - 4:
                excerpt += " ..."

        else:
            # if term is appearing in title, just leave normal body text
            excerpt = body_text[:180] + " ..."

    else:
        excerpt = body_text

    return excerpt
