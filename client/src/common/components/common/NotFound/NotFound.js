import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

import Cover from '../Cover';


const expressionsOfWoe = [
  'ἰὼ ἰώ',
  'αἶ, αἶ',
  'οἴμοι μοι',
  'φεῦ φεῦ',
  'ἰώ μοί μοι',
  'ὦ Ζεῦ',
  'βοᾷ βοᾷ',
  'αἰαῖ αἰαῖ',
  'ἔα ἔα',
  'ὀττοτοτοτοτοῖ',
  'ἄλγος ἄλγος βοᾷς',
  'ἐλελεῦ',
  'μὴ γένοιτο',
  'οὐαί'
];

const NotFound = props => {
	const leadContent = [];
  // hack spacing into this to display with default cover component
	leadContent.push(<br />);
  leadContent.push('We followed many twists and turns but were unable to find this work. ');
	leadContent.push(<br />);
	leadContent.push(<br />);
	leadContent.push(<a href="/">Make your odyssey back to the home page.</a>);
	leadContent.push(<br />);
	leadContent.push(<br />);
  let randomExpression = expressionsOfWoe[Math.floor(Math.random() * expressionsOfWoe.length)];

	return (
		<div className="notFound">

			<Cover
        title={`${randomExpression}!`}
				imageUrl="/static/img/sisyphus.gif"
				lead={leadContent}
				center
			/>

			<div className="notFoundText">
			</div>

		</div>
	);
};

export default NotFound;
