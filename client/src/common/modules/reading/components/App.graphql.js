/**
 * @prettier
 */

import { gql } from '@apollo/client';

const TEXT_NODE_FRAGMENT = gql`
	fragment AppTextNode on TextNode {
		id
		index
		location
		text
		urn
	}
`;

const WORK_FRAGMENT = gql`
	fragment AppWork on Work {
		id
		slug
		english_title
		original_title
		form
		work_type
		label
		description
		urn
		full_urn
		language {
			id
			title
			slug
		}
		version {
			id
			slug
			title
			description
		}
		exemplar {
			id
			slug
			title
			description
		}
		refsDecls {
			description
			label
		}
		translation {
			id
			slug
			title
			description
		}
	}
`;

const APP_INIT_QUERY = gql`
	query appInitQuery(
		$endsAtLocation: [Int]
		$fullUrnWithoutPassageCitation: String!
		$startsAtLocation: [Int]
		$textGroupUrn: CtsUrn
	) {
		textGroups(urn: $textGroupUrn) {
			id
			title
		}

		workByUrn(full_urn: $fullUrnWithoutPassageCitation) {
			...AppWork

			textNodes(
				startsAtLocation: $startsAtLocation
				endsAtLocation: $endsAtLocation
			) {
				...AppTextNode
			}
		}
	}
	${WORK_FRAGMENT}
	${TEXT_NODE_FRAGMENT}
`;

export const USER_GENERATED_CONTENT_QUERY = gql`
	query userGeneratedContentQuery($workUrn: String!) {
		commentCounts(urn: $workUrn) 
		@rest(type: "CommentCounts", path: "comments/counts_by_urn/?urn_search={args.urn}") {
			results
		}

		userTexts(urn: $workUrn) 
		@rest(type: "UserText", path: "usertexts/?urn_search={args.urn}") {
			results
		}
	}
`;

export default APP_INIT_QUERY;
