/**
 * @prettier
 */

const getCitationFromURN = (urn) =>
	urn.split(':').pop();

export default getCitationFromURN;
