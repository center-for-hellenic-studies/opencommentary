/**
 * @prettier
 */

import gql from 'graphql-tag';

const updateCommentMutation = gql`
	mutation commentUpdate($comment: CommentInputType!) {
		commentUpdate(comment: $comment) {
			_id
		}
	}
`;

export default updateCommentMutation;
