/**
 * @prettier
 */

import gql from 'graphql-tag';

const removeCommentMutation = gql`
	mutation commentRemove($hostname: String, $_id: String!) {
		commentRemove(_id: $_id, hostname: $hostname) {
			result
		}
	}
`;

export default removeCommentMutation;
