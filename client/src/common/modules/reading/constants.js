import { createContext } from 'react';

export const SET_CURRENT_COMMENT = '@@READING_ENV/SET_CURRENT_COMMENT';
export const SET_REVISIONS_MODAL_STATE = '@@READING_ENV/SET_REVISIONS_MODAL_STATE';
export const SET_SIDE_PANEL_STATE = '@@READING_ENV/SET_SIDE_PANEL_STATE';

export const ReadingEnvDispatch = createContext(null);
