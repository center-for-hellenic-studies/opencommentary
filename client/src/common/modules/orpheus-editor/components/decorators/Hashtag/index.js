import HashtagDecorator, { Hashtag, findHashtagEntities } from './Hashtag';
export { Hashtag, findHashtagEntities };
export default HashtagDecorator;
