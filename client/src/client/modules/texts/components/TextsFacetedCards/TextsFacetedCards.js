/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import autoBind from 'react-autobind';
import Sticky from 'react-stickynode';
import qs from 'qs-lite';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';

import List from '../../../../components/common/lists/List';
import TextListContainer from '../../containers/TextsListContainer';
import ArchiveFiltersContainer from '../../containers/TextsFiltersContainer';


class TextsFacetedCards extends React.Component {
	constructor(props) {
		super(props);

		const query = qs.parse(window.location.search.replace('?', ''));

		let workTitleSearchValue = '';
		if (query.search) {
			workTitleSearchValue = query.search;
		}

		this.state = {
			workTitleSearchValue,
		};
		autoBind(this);
	}

	componentDidMount() {
		this.collection = JSON.parse(
			document.getElementById('collection').textContent
		);
	}

	handleTextSearchWorkTitle() {
		const query = qs.parse(window.location.search.replace('?', ''));
		query.page = 1;
		query.search = this.state.workTitleSearchValue;
		window.history.replaceState(query, '', `?${qs.stringify(query)}`);
	}

	handleUpdateWorkSearchTitle(e) {
		this.setState({
			workTitleSearchValue: e.target.value,
		});
	}

	render() {
		let filterLookup = {};
		let items = [];

		filterLookup.language = {
			name: 'Language',
			type: 'text',
			values: [],
		};
		filterLookup.work_type = {
			name: 'Work Type',
			type: 'text',
			values: [],
		};
		filterLookup.structure = {
			name: 'Structure',
			type: 'text',
			values: [],
		};
		filterLookup.collection = {
			name: 'Collection',
			type: 'text',
			values: [],
		};
		filterLookup.textGroup = {
			name: 'Author',
			type: 'text',
			values: [],
		};

		const collectionJSON =  JSON.parse(collection.textContent)

		// build filters
		const collections = collectionJSON.collections;
		collections.forEach(col =>{
			filterLookup.collection.values.push(col.title);
		});
		const textGroups = collectionJSON.text_groups;
		textGroups.forEach(tg => {
			filterLookup.textGroup.values.push(tg.title);
		});
		const languages = collectionJSON.languages;
		languages.forEach(lang => {
			filterLookup.language.values.push(lang.title);
		});

		const relevantWorks = collectionJSON.works;
		relevantWorks.forEach(text => {
			text.type = 'text';

			text.works.forEach(work => {
				if (
					work.work_type &&
					work.work_type.length > 0 &&
					!filterLookup.work_type.values.includes(work.work_type)
				) {
					filterLookup.work_type.values.push(work.work_type);
				}

				if (
					work.structure &&
					work.structure.length > 0 &&
					!filterLookup.structure.values.includes(work.structure)
				) {
					filterLookup.structure.values.push(work.structure);
				}
			});

			items.push(text);
		});

		const filters = Object.keys(filterLookup).map(filter_name => {
			return filterLookup[filter_name];
		});

		const _classes = this.props.classes || [];

		_classes.push('facetedCards');

		if (this.props.loading) {
			_classes.push('-loading');
		}

		return (
			<div className={_classes.join(' ')}>
				<h5>Texts /</h5>
				<h2>Commentary Texts</h2>
				<div className="facetedCardsContent">
					<Sticky
						className="facetedCardsContentFilters"
						activeClass="-sticky"
						bottomBoundary=".sticky-reactnode-boundary"
						enabled
					>
						<div className="facetedCardsContentFiltersInner">
							<div className="readingEnvMenu workSearch">
								<form onSubmit={this.handleTextSearchWorkTitle}>
									<TextField
										id="workTitleSearch"
										name="search"
										label="Search for a text . . ."
										fullWidth
										variant="outlined"
										onChange={this.handleUpdateWorkSearchTitle.bind(this)}
										value={this.state.workTitleSearchValue}
									/>
									<button
										className="alexandriaButton"
									>
										<SearchIcon />
									</button>
								</form>
							</div>
							{this.props.loading ? (
								<ArchiveFiltersContainer loading />
							) : (
								<ArchiveFiltersContainer filters={filters} items={items}/>
							)}
						</div>
					</Sticky>
					<div className="facetedCardsContentCards">
						{this.props.items ? (
							<List
								loading={this.props.loading}
								selectable={this.props.selectable}
								items={this.props.items}
								isAdmin={this.props.isAdmin}
							/>
						) : (
							<TextListContainer
								loading={this.props.loading}
								selectable={this.props.selectable}
								isAdmin={this.props.isAdmin}
								items={items}
							/>
						)}
					</div>
				</div>
			</div>
		);
	}
}

TextsFacetedCards.propTypes = {
	classes: PropTypes.arrayOf(PropTypes.string),
	isAdmin: PropTypes.bool,
	items: PropTypes.array,
	loading: PropTypes.bool,
	selectable: PropTypes.bool,
	theme: PropTypes.string,
};

TextsFacetedCards.defaultProps = {
	theme: '',
};

export default TextsFacetedCards;
