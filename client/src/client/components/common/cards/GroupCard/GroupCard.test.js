import React from 'react';
import { shallow } from 'enzyme';

// component
import GroupCard from './GroupCard';

describe('GroupCard', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<GroupCard to="/"/>);
		expect(wrapper).toBeDefined();
	});
});
