import React from 'react';
import { shallow } from 'enzyme';

// component
import FileUploader from './FileUploader';

describe('FileUploader', () => {
	it('renders correctly', () => {

		const wrapper = shallow(
			<FileUploader handleAddFiles={() => {}} />
		);
		expect(wrapper).toBeDefined();
	});
});
