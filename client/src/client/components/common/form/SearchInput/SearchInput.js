import React from 'react';

import SearchIcon from '@material-ui/icons/Search';
import { IconButton, InputBase } from '@material-ui/core';

const SearchInput = props => (
	<div className="searchInput">
		<InputBase placeholder="Search" />
		<IconButton aria-label="Search">
			<SearchIcon />
		</IconButton>
	</div>
);

export default SearchInput;
