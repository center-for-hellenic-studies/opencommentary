import React from 'react';
import { shallow } from 'enzyme';

// component
import Facet from './Facet';

describe('Facet', () => {
	it('renders correctly', () => {

		const wrapper = shallow(<Facet />);
		expect(wrapper).toBeDefined();
	});
});
