/**
 * @prettier
 */

import React from 'react';
import PropTypes from 'prop-types';
import Sticky from 'react-stickynode';
import cn from 'classnames';

import List from '../lists/List';
// import ItemListContainer from '../../../modules/items/containers/ItemListContainer';
// import ArchiveFiltersContainer from '../../../modules/archives/containers/ArchiveFiltersContainer';

function FacetedCards({ classes, isAdmin, items, loading, selectable }) {
	const _classes = cn(classes, 'facetedCards', {
		'-loading': loading,
	});

	return (
		<div className={_classes}>
			<div className="facetedCardsContent">
				<Sticky
					className="facetedCardsContentFilters"
					activeClass="-sticky"
					bottomBoundary=".sticky-reactnode-boundary"
					enabled
				>
					{/* <div className="facetedCardsContentFiltersInner">
						<ArchiveFiltersContainer loading={loading} />
					</div> */}
				</Sticky>
				<div className="facetedCardsContentCards">
					{items && (
						<List
							loading={loading}
							selectable={selectable}
							items={items}
							isAdmin={isAdmin}
						/>
					)}
					{/* <ItemListContainer
							loading={loading}
							selectable={selectable}
							isAdmin={isAdmin}
						/> */}
				</div>
			</div>
		</div>
	);
}

FacetedCards.propTypes = {
	classes: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.arrayOf(PropTypes.string),
	]),
	isAdmin: PropTypes.bool,
	items: PropTypes.array,
	loading: PropTypes.bool,
	selectable: PropTypes.bool,
	selectable: PropTypes.bool,
};

export default FacetedCards;
