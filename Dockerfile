# syntax=docker/dockerfile:1
FROM python:3.9-alpine AS base

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /code

RUN \
    apk add --no-cache postgresql-libs && \
    apk add --no-cache --virtual .build-deps gcc python3-dev musl-dev postgresql-dev && \
    apk add jpeg-dev zlib-dev libjpeg && \
    apk add --update --no-cache g++ gcc libxslt-dev

COPY requirements.txt ./
COPY alexandria_app ./alexandria_app
RUN pip install -r requirements.txt && apk --purge del .build-deps

######
# Anything following this line should not run in development because the
# docker-compose.dev.yml specifies `base` as the build target.
######

FROM node:lts-alpine AS statics

ARG NODE_ENV=production
ARG REACT_APP_BUCKET_URL=https://s3.amazonaws.com/iiif-orpheus
ARG REACT_APP_EMBEDLY_API_KEY=502d8f3eb6044135a8a06d4f91d04ba3
ARG REACT_APP_GRAPHQL_URL=https://texts.newalexandria.info/graphql
ARG REACT_APP_IS_ON_SERVER=server
ARG REACT_APP_SERVER=https://oc.newalexandria.info
ARG STATIC_URL=https://ktemata.newalexandria.info/opencommentary/

WORKDIR /code/client

COPY ./client .

RUN yarn install

RUN NODE_ENV=${NODE_ENV} REACT_APP_IS_ON_SERVER=${REACT_APP_IS_ON_SERVER} yarn node --max-old-space-size=1000 scripts/build.js

FROM base AS production

WORKDIR /code

COPY --from=statics /code/client/build /code/client/build
COPY --from=statics /code/client/public /code/client/public
COPY --from=statics /code/client/webpack-stats.json /code/client/webpack-stats.json

COPY ./alexandria_app ./alexandria_app/
COPY ./config ./config/
COPY ./scripts ./scripts/
COPY ./manage.py ./
COPY ./start.sh ./

RUN chmod +x ./start.sh

ARG DO_S3_ACCESS_KEY
ARG DO_S3_SECRET_KEY

RUN python manage.py collectstatic --no-input

ENTRYPOINT ["./start.sh"]
